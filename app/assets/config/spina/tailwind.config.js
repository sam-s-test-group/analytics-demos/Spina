module.exports = {
  content: [
    '/Users/stkerr/Code/Spina/app/views/**/*.*',
'/Users/stkerr/Code/Spina/app/components/**/*.*',
'/Users/stkerr/Code/Spina/app/helpers/**/*.*',
'/Users/stkerr/Code/Spina/app/assets/javascripts/**/*.js',
'/Users/stkerr/Code/Spina/app/**/application.tailwind.css'
  ],
  theme: {
    fontFamily: {
      body: ['Metropolis'],
      mono: ['ui-monospace', 'SFMono-Regular', 'Menlo', 'Monaco', 'Consolas', "Liberation Mono", "Courier New", 'monospace']
    },
    extend: {
      colors: {
        spina: {
          light: '#797ab8',
          DEFAULT: '#6865b4',
          dark: '#3a3a70'
        }
      }
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
	require('@tailwindcss/aspect-ratio'),
	require('@tailwindcss/typography')
  ]
}
